import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue2'
import path from 'path'

export default defineConfig({
  plugins: [
    vue(),
  ],
  server: {
    port: 8080, // <-- Set your own server port here
    proxy: {
      '/api': {
        target: process.env.API_TARGET || 'http://localhost:1323',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, 'api')
      }
    }
  },
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: `@import "@/variables.scss";`
      }
    }
  },
  resolve: {
    extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json', '.vue'],
    alias: [
      {
        find: '@',
        replacement: path.resolve(__dirname, 'src')
      }
    ]
  },
  build: {
    chunkSizeWarningLimit: 700, // Default is 500
    rollupOptions: {
      external: ['/api/data/medias/th-avatar.png']
    }
  },
})
