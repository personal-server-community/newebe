import MarkdownIt from 'markdown-it'
import moment from 'moment'


export default {
  markdownToHtml (mdText) {
    const md = new MarkdownIt()
    return md.render(mdText)
  },

  formatDate (date) {
    const postDate = moment(date, "YYYY-MM-DDThh:mm")
    const yesterday = moment().add(-1, 'days')
    if (postDate.isBefore(yesterday)) {
      return postDate.format('DD MMMM YYYY HH:mm')
    } else {
      return postDate.fromNow()
    }
  }
}
