export default {

  buildThumbnailPath (fileName) {
    const folderName = fileName.substring(0, 10)
    return `/api/data/medias/${folderName}/th-${fileName}`
  },

  buildImagePath (fileName) {
    const folderName = fileName.substring(0, 10)
    return `/api/data/medias/${folderName}/${fileName}`
  },

  buildVideoPath (fileName) {
    const folderName = fileName.substring(0, 10)
    return `/api/data/medias/${folderName}/${fileName}`
  }
}
