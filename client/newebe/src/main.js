import Vue from 'vue'
import infiniteScroll from 'vue-infinite-scroll'
import './assets/tailwind.css'

import App from '@/App.vue'
import router from '@/router'
import store from '@/store'

/* Icons */
import Unicon from 'vue-unicons'
import {
  uniCommentsAlt,
  uniGlobe,
  uniScenery,
  uniUsersAlt,
  uniSignOutAlt,
  uniUser
} from 'vue-unicons/src/icons'

Unicon.add([
  uniCommentsAlt,
  uniGlobe,
  uniScenery,
  uniSignOutAlt,
  uniUser,
  uniUsersAlt
])
Vue.use(Unicon)

// Directives
Vue.use(infiniteScroll)

/* Start Vue */
Vue.config.productionTip = false
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
