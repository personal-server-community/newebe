import axios from 'axios'
import Vue from 'vue'
import VueNativeSock from 'vue-native-websocket'
const client = axios.create({
	headers: {
		"Content-Type": "application/json",
  }
})

client.getFromApi = (subPath) => {
  return client
    .get(`/api/${subPath}`)
    .then(response => {
      return Promise.resolve(response.data)
    })
}

client.getStatus = () => {
  return client.getFromApi('status')
}

// Auth

client.isAuthenticated = () => {
  const token = localStorage.getItem('auth-token')
  if (!client.defaults.headers.common.Authorization) {
    client.defaults.headers.common.Authorization = 'Bearer ' + token
  }
  return client.getFromApi('data/authenticated')
    .then(() => {
      /* Websocket */
      const host = window.location.hostname
      const port = window.location.port
      const protocol = window.location.protocol === 'https:' ? 'wss' : 'ws'
      const wsurl = `${protocol}://${host}:${port}/api/events?token=${token}`
      Vue.use(VueNativeSock, wsurl, { format: 'json' })
			return Promise.resolve()
    })
    .then(() => Promise.resolve(true))
    .catch(() => Promise.resolve(false))
}

client.postLogin = (form) => {
  const data = {
    password: form.password
  }
  return client
    .post("/api/login", data)
    .then(response => {
      const token = response.data.token
      localStorage.setItem('auth-token', token)
      client.defaults.headers.common.Authorization = 'Bearer ' + token
      /* Websocket */
      const host = window.location.hostname
      const port = window.location.port
      const wsurl = `ws://${host}:${port}/api/events?token=${token}`
      Vue.use(VueNativeSock, wsurl, { format: 'json' })
      return Promise.resolve(response.data)
    })
}

client.logout = () => {
  return client.delete("/api/logout")
}

client.postRegister = (form) => {
  const data = {
    name: form.name,
    url: form.url,
    password: form.password,
  }
  return client
    .post("/api/register", data)
    .then((response) => {
      return Promise.resolve(response.data)
    })
}

// Profile

client.getProfile = () => {
  return client.getFromApi('data/profile')
}

client.putProfile = (profile) => {
  return client
    .put("/api/data/profile", profile)
    .then((response) => {
      const message = response.data
      return Promise.resolve(message)
    })
}

client.uploadAvatar = (formData) => {
  client.post('/api/data/avatar', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// Messages

client.getMessages = (page) => {
  return client.getFromApi('data/messages?page=' + page)
}

client.getAttachments = () => {
  return client.getFromApi('data/attachments')
}

client.postMessage = (content, files) => {
  if (files) {
    const form = new FormData()
    let i = 0
    files.forEach(file => {
      form.append('file-' + i, file.get('file'))
      i++
    })
    form.set('content', content)
    return client
      .post("/api/data/messages/with-attachments", form)
      .then(response => {
        const message = response.data
        return Promise.resolve(message)
      })
  } else {
    const data = { content }
    return client
      .post("/api/data/messages", data)
      .then((response) => {
        const message = response.data
       return Promise.resolve(message)
    })
  }
}

client.deleteMessage = (message) => {
  return client.delete('/api/data/messages/' + message.uuid)
}

client.getContacts = () => {
  return client.getFromApi('data/contacts')
}

client.postContact = (url) => {
  const data = { url }
  return client.post('/api/data/contacts', data)
}

client.postConfirmContact = (id) => {
  return client.post('/api/data/contacts/' + id + '/confirm', {})
}

client.deleteContact = (id) => {
  return client.delete('/api/data/contacts/' + id)
}

export default client
