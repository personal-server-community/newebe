import client from '@/api/client'
import { partition } from 'lodash'

import {
  ADD_ATTACHMENT,
  ADD_CONTACT,
  ADD_MESSAGE,
  CHANGE_CONTACT_STATUS,
  DELETE_MESSAGE,
  REMOVE_CONTACT,
  RESET_ALL,
  STORE_ATTACHMENTS,
  STORE_MORE_ATTACHMENTS,
  STORE_CONTACTS,
  STORE_MESSAGES,
  STORE_MORE_MESSAGES,
  STORE_PROFILE,
  UPDATE_PROFILE,
  TOGGLE_DARK_THEME
} from '@/store/mutation-types'

const initialState = {
  attachments: [],
  contacts: [],
  contactMap: {},
  isDarkTheme: false,
  messages: [],
  messageMap: {},
  profile: {}
}

const state = { ...initialState }

const getters = {
  attachments: state => state.attachments,
  attachmentsByDay: state => partition(state.attachments, 'day'),
  activeContacts: state => state.contacts.filter(c => c.status !== 'DELETED'),
  contacts: state => state.contacts,
  contactMap: state => state.contactMap,
  isDarkTheme: state => state.isDarkTheme,
  messages: state => state.messages,
  profile: state => state.profile
}

const actions = {
  login (payload, credentials) {
    return client.postLogin(credentials)
  },

  logout ({ commit }) {
    commit(RESET_ALL)
    return client.logout()
  },

  register (payload, data) {
    const profile = {
      name: data.name,
      password: data.password,
      url: data.url
    }
    return client.postRegister(profile)
  },

  loadData ({ dispatch }) {
    return dispatch('fetchProfile')
      .then(() => dispatch('fetchContacts'))
      .then(() => dispatch('fetchMessages'))
  },

  fetchProfile ({ commit }) {
    return client
      .getProfile()
      .then((profile) => {
        commit(STORE_PROFILE, profile)
        Promise.resolve(profile)
      })
      .catch(console.error)
  },

  saveProfile ({ commit }, data) {
    const profile = {
      name: data.name,
      url: data.url
    }
    commit(UPDATE_PROFILE, profile)
    return client.putProfile(data)
  },

  uploadAvatar (context, formData) {
    return client.uploadAvatar(formData)
  },

  toggleDarkTheme ({ commit, state }) {
    commit(TOGGLE_DARK_THEME)
    if (localStorage) {
      localStorage.setItem('dark-theme', state.isDarkTheme)
    }
  },

  fetchStatus () {
    return client
      .getStatus()
      .catch(console.error)
  },

  fetchAuthenticated () {
    return client
      .isAuthenticated()
  },

  fetchMessages ({ commit }, page = 0) {
    return client
      .getMessages(page)
      .then(result => {
        if (page === 0) commit(STORE_MESSAGES, result)
        else commit(STORE_MORE_MESSAGES, result)
        return Promise.resolve(result.messages)
      })
      .then(client.getAttachments)
      .then(attachments => {
        if (page === 0) commit(STORE_ATTACHMENTS, attachments)
        else commit(STORE_MORE_ATTACHMENTS, attachments)
        return Promise.resolve(attachments)
      })
      .catch(console.error)
  },

  addMessage ({ commit }, message) {
    commit(ADD_MESSAGE, message)
  },

  addAttachment ({ commit }, attachment) {
    commit(ADD_ATTACHMENT, attachment)
  },

  postMessage (context, { content, form }) {
    return client.postMessage(content, form)
  },

  removeMessage ({ commit }, message) {
    commit(DELETE_MESSAGE, message)
    client.deleteMessage(message)
      .catch(console.error)
  },

  fetchContacts ({ commit }) {
    return client
      .getContacts()
      .then((contacts) => {
        commit(STORE_CONTACTS, contacts)
        return Promise.resolve(contacts)
      })
      .catch(console.error)
  },

  addContact ({ commit }, contact)  {
    commit(ADD_CONTACT, contact)
  },

  createContact (payload, url)  {
    return client
      .postContact(url)
  },

  removeContact ({ commit }, contact)  {
    commit(REMOVE_CONTACT, contact)
  },

  changeContactStatus ({ commit }, contact)  {
    commit(CHANGE_CONTACT_STATUS, contact)
  },

  confirmContact (payload, contact)  {
    return client
      .postConfirmContact(contact.uuid)
      .catch(console.error)
  },

  deleteContact (payload, contact)  {
    return client
      .deleteContact(contact.uuid)
      .catch(console.error)
  }
}

const mutations = {
  [TOGGLE_DARK_THEME] (state) {
    state.isDarkTheme = !state.isDarkTheme
  },

  [STORE_PROFILE] (state, profile) {
    state.profile = profile || {}
  },

  [STORE_CONTACTS] (state, contacts) {
    state.contacts = contacts || []
    state.contacts.forEach(contact => {
      state.contactMap[contact.key] = contact
    })
  },

  [ADD_CONTACT] (state, contact) {
    state.contacts.push(contact)
  },

  [CHANGE_CONTACT_STATUS] (state, contact) {
    const localContact = state.contacts.find(c => c.uuid === contact.uuid)
    localContact.status = contact.status
  },

  [REMOVE_CONTACT] (state, contact) {
    state.contacts = state.contacts.filter(c => c.uuid !== contact.uuid)
  },

  [STORE_MESSAGES] (state, { messages, attachments }) {
    messages.forEach(m => state.messageMap[m.uuid] = m)
    attachments.forEach(a => {
      if (!state.messageMap[a.postId].attachments) {
        state.messageMap[a.postId].attachments = []
      }
      state.messageMap[a.postId].attachments.push(a)
    })
    state.messages = messages
  },

  [STORE_MORE_MESSAGES] (state, { messages, attachments }) {
    if (!messages) return
    messages.forEach(m => state.messageMap[m.uuid] = m)
    attachments.forEach(a => {
      state.messageMap[a.postId].attachments = [a] }
    )
    state.messages = state.messages.concat(messages)
  },

  [STORE_ATTACHMENTS] (state, attachments) {
    attachments.forEach(a => {
      a.day = a.createdAt.substring(10, 0)
    })
    state.attachments = attachments
  },

  [STORE_MORE_ATTACHMENTS] (state, attachments) {
    if (!attachments) return
    attachments.forEach(a => {
      a.day = a.createdAt.substring(10, 0)
    })
    state.attachments = state.attachments.concat(attachments)
  },

  [ADD_MESSAGE] (state, message) {
    message.attachments = []
    state.messageMap[message.uuid] = message
    state.messages.unshift(message)
  },

  [ADD_ATTACHMENT] (state, attachment) {
    const message = state.messageMap[attachment.postId]
    if (message) {
      if (!message.attachments) message.attachments = []
      attachment.day = attachment.createdAt.substring(10, 0)
      state.attachments.unshift(attachment)
      message.attachments.push(attachment)
      message.attachments.sort((a, b) => a.index < b.index)
    }
  },

  [DELETE_MESSAGE] (state, message) {
    state.messages = state.messages.filter(m => m.uuid != message.uuid)
  },

  [UPDATE_PROFILE] (state, data) {
    Object.assign(state.profile, data)
  },

  [RESET_ALL] (state) {
    const isDarkTheme = state.isDarkTheme
    Object.assign(state, { ...initialState })
    state.isDarkTheme = isDarkTheme
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
