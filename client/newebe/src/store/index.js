import Vue from 'vue'
import Vuex from 'vuex'

import context from './context'

Vue.use(Vuex)

const modules = {
  context
}

export default new Vuex.Store({
  strict: true,
  modules: modules
})
