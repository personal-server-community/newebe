import Contacts from '@/components/Contacts.vue'
import Login from '@/components/Login.vue'
import MainWrapper from '@/components/Main.vue'
import Messages from '@/components/Messages.vue'
import Profile from '@/components/Profile.vue'
import Pictures from '@/components/Pictures.vue'
import Register from '@/components/Register.vue'

export const routes = [
  {
    path: '',
    component: MainWrapper,
		children: [
			{
				path: '',
				component: Messages,
				name: 'home'
			},
			{
				path: '',
				component: Messages,
				name: 'messages'
			},
			{
				path: 'register',
				component: Register,
				name: 'register'
			},
			{
				path: 'login',
				component: Login,
				name: 'login'
			},
			{
				path: 'contacts',
				component: Contacts,
				name: 'contacts'
			},
			{
				path: 'profile',
				component: Profile,
				name: 'profile'
			},
			{
				path: 'pictures',
				component: Pictures,
				name: 'pictures'
			}
		]
	}
]
