.PHONY: setup
setup: ## Install all the build and lint dependencies
	go get -u golang.org/x/tools/cmd/cover
	@$(MAKE) dep

.PHONY: dep
dep: ## Run get all required files
	@echo ">> updating Go dependencies"
	@for m in $$(go list -mod=readonly -m -f '{{ if and (not .Indirect) (not .Main)}}{{.Path}}{{end}}' all); do \
		go get $$m; \
	done
	go mod tidy

.PHONY: test
test: ## Run all the tests
	echo 'mode: atomic' > coverage.txt && go test -covermode=atomic -coverprofile=coverage.txt -v -race -timeout=30s ./...

.PHONY: cover
cover: test ## Run all the tests and opens the coverage report
	go tool cover -html=coverage.txt

.PHONY: fmt
fmt: ## Run goimports on all go files
	find . -name '*.go' -not -wholename './vendor/*' | while read -r file; do ~/go/bin/goimports -w "$$file"; done

.PHONY: lint
lint: ## Run all the linters
	~/go/bin/golangci-lint run

.PHONY: ci
ci: lint test ## Run all the tests and code checks

.PHONY: build
build: ## Build a version
	go build -o newebe

.PHONY: clean
clean: ## Remove temporary files
	go clean

.PHONY: client
client: ## Build frontend
	cd client/newebe && npm run build

.PHONY: docker
docker: ## Build docker image
	cd docker && DOCKER_BUILDKIT=1 sudo docker buildx build  --platform linux/amd64,linux/arm/v6 . --no-cache -t personalserverorg/newebe

.PHONY: push-docker
push-docker: ## Publish docker image
	cd docker && DOCKER_BUILDKIT=1 sudo docker buildx build  --platform linux/amd64,linux/arm64,linux/armv6,linux/arm/v6 . --no-cache -t personalserverorg/newebe:latest --push
# Absolutely awesome: http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := build
