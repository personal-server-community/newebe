# Newebe, Freedom To Share

Newebe is a social network dedicated to your close relations. Think it as a
place to post interesting stuff and news for your closed ones.

It's based on a peer-to-peer architecture. Every member must host his own
instance. All your conversations are owned by you and are not used by a
third-party.

This project is still a work in progress.

## Setup (Ubuntu)

Install docker: https://docs.docker.com/engine/install/ubuntu/

To run Newebe, the best option is to run the Docker image:

```
sudo docker pull personalserverorg/newebe
docker run --init -ti --rm -p 80:80 --name newebe \
-e AUTH_SECRET=secret1 \
personalserverorg/newebe
```

Use Letsencrypt certificate: https://certbot.eff.org/instructions?ws=nginx&os=ubuntufocal

Nginx conf if you want to set it up behind a proxy:

```
server {
    listen 80;
    server_name yournewebe.yoururl.net;
    return 301 https://$host$request_uri;
}

server {
    listen 443 ssl;
    server_name yournewebe.yoururl.net;
    ssl_certificate      /etc/letsencrypt/live/yoururl.net/cert.pem;
    ssl_certificate_key  /etc/letsencrypt/live/yoururl.net/privkey.pem;

    location / {
        proxy_set_header Host $http_host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_pass http://localhost:9000;
    }

    location /api/events {
        proxy_http_version 1.1;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
        proxy_pass http://localhost:9000/api/events;
    }
}

```

## Contribute

As any open source project, we enjoy any contribution! You will find below how
you can help.

### Bug reports and feature requests

All bugs and feature requests must be submitted directly in the issue page of
this repository.

If you want to contribute to translations, you can connect directly to the
POEditor platform.

### Code

All contributions are welcomed as long as they respect the 
[C4 contract](https://rfc.zeromq.org/spec/42/).

Development environment setup: TODO


## About

This project is part of the 
[Personal Server Community](http://personal-server.org)
