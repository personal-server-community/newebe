package database

import (
	"time"
)

// User profile data
type User struct {
	Uuid      string    `storm:"id"`
	CreatedAt time.Time `storm:"index"`
	UpdatedAt time.Time `storm:"index"`
	Name      string    `storm:"index"`
	Url       string    `storm:"unique"`
	Key       string    `storm:"unique"`
	Password  []byte
	HasAvatar bool `json:"hasAvatar"`
}

// Auth token storage
type AuthToken struct {
	Uuid       string    `storm:"id"`
	CreatedAt  time.Time `storm:"index"`
	Token      string    `storm:"index,unique"`
	Expiration time.Time ``
}

// Table to store messages
type Post struct {
	Uuid          string    `storm:"id"`
	CreatedAt     time.Time `storm:"index"`
	UpdatedAt     time.Time `storm:"index"`
	Content       string    ``
	AuthorKey     string    `storm:"index"`
	HasAttachment bool      `storm:"index"`
}

// Table to store attachments metadata
type Attachment struct {
	Uuid      string    `storm:"id" json:"uuid"`
	CreatedAt time.Time `storm:"index" json:"createdAt"`
	PostId    string    `storm:"index" json:"postId"`
	Size      int64     `json:"size"`
	FileName  string    `json:"fileName"`
	Extension string    `json:"extension"`
	Index     int       `json:"index"`
}

// Table to store contacts
type Contact struct {
	Uuid      string    `storm:"id"`
	CreatedAt time.Time `storm:"index"`
	UpdatedAt time.Time `storm:"index"`
	Name      string    `storm:"index"`
	Url       string    `storm:"unique"`
	Key       string    `storm:"unique"`
	LocalKey  string    `storm:"unique"`
	Status    string    `storm:"index"`
	HasAvatar bool      ``
}
