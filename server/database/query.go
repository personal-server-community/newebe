package database

import (
	"errors"
	"fmt"
	"image/jpeg"
	"io"
	"mime/multipart"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/asdine/storm/q"
	"github.com/disintegration/imaging"
	guuid "github.com/google/uuid"
	"gitlab.com/personal-server-community/newebe/server/data"
)

const LAYOUT_DATE = "2006-01-02"
const LAYOUT_DATETIME = "2006-01-02-15:04:05"

func GetAuthTokens() ([]AuthToken, error) {
	var tokens []AuthToken
	err := Db.All(&tokens)
	if err != nil {
		return nil, err
	}
	return tokens, nil
}

// Retrieve the list of users
// NB: For the moment Newebe handles a single user.
func GetUsers() ([]User, error) {
	var users []User
	err := Db.All(&users)
	if err != nil {
		return nil, err
	}
	return users, nil
}

// Retrieve Newebe main user.
// NB: For the moment Newebe handles a single user.
func GetUser() (User, error) {
	var users []User
	err := Db.All(&users)
	if err != nil {
		return User{}, err
	} else if len(users) > 0 {
		return users[0], nil
	} else {
		return User{}, errors.New("No user in the database")
	}
}

// Generate a new user in the database. If there is already an user, it returns
// it. For the moment Newebe handles only one user.
// We assume here that the password has already been hashed.
func CreateUser(name string, url string, password []byte) (User, error) {
	users, err := GetUsers()
	if err != nil {
		return User{}, err
	}
	if len(users) == 0 {
		user := User{
			Uuid:      guuid.New().String(),
			Name:      name,
			Url:       url,
			Password:  password,
			CreatedAt: time.Now(),
		}
		err := Db.Save(&user)
		if err != nil {
			return User{}, err
		}
		return user, nil
	} else {
		return users[0], nil
	}
}

// Update user's data.
func UpdateUser(profile data.Profile) (User, error) {
	user, err := GetUser()
	if err != nil {
		return User{}, err
	}
	user.Name = profile.Name
	user.Url = profile.Url
	err = Db.Save(&user)
	if err != nil {
		return User{}, err
	}
	return user, nil
}

// Set user has avatar flag to true.
func SetHasAvatar(user User) error {
	user.HasAvatar = true
	err := Db.Save(&user)
	if err != nil {
		return err
	}
	return nil
}

// Check if a token is present in the database. It returns if it is found.
func GetAuthToken(token string) (AuthToken, error) {
	var authtoken AuthToken
	err := Db.One("Token", token, &authtoken)
	if err != nil {
		return AuthToken{}, err
	}
	return authtoken, nil
}

// Create a new session token entry.
func CreateAuthToken(token string) (AuthToken, error) {
	authtoken := AuthToken{
		Uuid:       guuid.New().String(),
		Token:      token,
		CreatedAt:  time.Now(),
		Expiration: time.Now().Add(time.Hour * 24 * 30),
	}
	err := Db.Save(&authtoken)
	if err != nil {
		return AuthToken{}, err
	}
	return authtoken, nil
}

// Delete a token, find it by its value.
func DeleteAuthToken(token string) error {
	authtoken, err := GetAuthToken(token)
	if err != nil {
		return nil
	}
	err = Db.DeleteStruct(&authtoken)
	return err
}

// Return attachments linked to given post IDs.
func GetAttachments(postIds []string) ([]Attachment, error) {
	var attachments []Attachment
	query := Db.
		Select(q.In("PostId", postIds)).
		OrderBy("CreatedAt").
		Reverse()
	err := query.Find(&attachments)
	if err != nil {
		return []Attachment{}, err
	}
	return attachments, nil
}

// Return last attachments created.
func GetLastAttachments() ([]Attachment, error) {
	var attachments []Attachment
	query := Db.Select().OrderBy("CreatedAt").Reverse()
	err := query.Find(&attachments)
	if err != nil {
		return []Attachment{}, err
	}
	return attachments, nil
}

// Retrieve all messages matching given page (10 messages per page).
func GetPosts(page int) ([]Post, error) {
	var posts []Post
	page_size := 10
	query := Db.Select().
		OrderBy("CreatedAt").
		Reverse().
		Skip(page_size * page).
		Limit(page_size)
	err := query.Find(&posts)
	if err != nil {
		return []Post{}, err
	}
	return posts, nil
}

// Retrieve given post.
func GetPost(uuid string) (Post, error) {
	var post Post
	err := Db.One("Uuid", uuid, &post)
	if err != nil {
		return Post{}, err
	}
	return post, nil
}

// Create a new message. If some attachment will be linked to it, mark it via
// the hasAttachment flag.
func CreatePost(content string, hasAttachment bool) (Post, error) {
	user, err := GetUser()
	if err != nil {
		return Post{}, err
	}
	post := Post{
		Uuid:          guuid.New().String(),
		AuthorKey:     user.Uuid,
		Content:       content,
		CreatedAt:     time.Now(),
		HasAttachment: hasAttachment,
	}
	err = Db.Save(&post)
	if err != nil {
		return Post{}, err
	}
	return post, nil
}

// Create a new message from incoming data (a message from one of the contact).
func CreateContactPost(message data.PeerContactMessage) (Post, error) {
	createdAt, err := time.Parse(time.RFC3339, message.CreatedAt)
	if err != nil {
		return Post{}, err
	}
	post := Post{
		Uuid:          message.Uuid,
		AuthorKey:     message.Key,
		Content:       message.Content,
		CreatedAt:     createdAt,
		HasAttachment: message.HasAttachment,
	}
	if err = Db.Save(&post); err != nil {
		return Post{}, err
	}
	return post, nil
}

// Create a new attachment from incoming data (an attachment from one of the
// contact).
func CreateContactAttachment(
	contactAttachment data.PeerContactAttachment,
) (Attachment, error) {
	createdAt, err := time.Parse(
		time.RFC3339,
		contactAttachment.CreatedAt,
	)
	if err != nil {
		return Attachment{}, err
	}
	attachment := Attachment{
		Uuid:      contactAttachment.Uuid,
		PostId:    contactAttachment.PostId,
		Size:      contactAttachment.Size,
		FileName:  contactAttachment.FileName,
		CreatedAt: createdAt,
	}
	if err = Db.Save(&attachment); err != nil {
		return Attachment{}, err
	}
	return attachment, nil
}

// Retrieve a contact by its key.
func GetContactByKey(key string) (Contact, error) {
	var contact Contact
	err := Db.One("Key", key, &contact)
	if err != nil {
		return Contact{}, err
	}
	return contact, nil
}

// Returns true if keys provided match a contact in the database.
func CheckContact(key string, localKey string) (Contact, error) {
	var contact Contact
	err := Db.One("Key", key, &contact)
	if err != nil {
		return Contact{}, err
	}
	if contact.LocalKey != localKey {
		return Contact{}, errors.New("Contact has wrong key")
	}
	return contact, nil
}

func saveFileInMedias(
	file *multipart.FileHeader,
	folderName string,
	fileName string,
) (int64, error) {
	mediasFolder := GetMediaFolder()
	dstFolder := mediasFolder + "/" + folderName
	dstPath := dstFolder + "/" + fileName
	dstThPath := dstFolder + "/th-" + fileName
	err := os.Mkdir(dstFolder, 0755)
	if err != nil && !os.IsExist(err) {
		return 0, err
	}
	extension := fileName[len(fileName)-3:]
	src, err := file.Open()
	if err != nil {
		return 0, err
	}
	defer src.Close()

	dst, err := os.Create(dstPath)
	if err != nil {
		return 0, err
	}

	if strings.ToLower(extension) == "mp4" {
		if _, err = io.Copy(dst, src); err != nil {
			return 0, err
		}
	} else {
		srcImage, err := imaging.Decode(src, imaging.AutoOrientation(true))
		thumbnail := imaging.Thumbnail(srcImage, 150, 150, imaging.Lanczos)
		dst, err := os.Create(dstPath)
		if err != nil {
			return 0, err
		}
		jpeg.Encode(dst, srcImage, nil)
		dstTh, err := os.Create(dstThPath)
		if err != nil {
			return 0, err
		}
		jpeg.Encode(dstTh, thumbnail, nil)
	}
	stats, err := dst.Stat()

	return stats.Size(), nil
}

// Save attachment: create an entry in the database and store file in the media
// folder following this path:
// 2021-10-08/2021-10-08-01:58:12-6d3e92d4-01dd-4774-8b9e-1697b9011263-0.jpg
func SaveAttachment(
	post Post,
	index int,
	file *multipart.FileHeader,
) (Attachment, error) {
	folderName := post.CreatedAt.Format(LAYOUT_DATE)
	date := post.CreatedAt.Format(LAYOUT_DATETIME)
	extension := file.Filename[len(file.Filename)-3:]
	fileName :=
		date + "-" + post.Uuid + "-" + strconv.Itoa(index) + "." + extension
	size, err := saveFileInMedias(file, folderName, fileName)
	if err != nil {
		return Attachment{}, err
	}
	attachment := Attachment{
		Uuid:      guuid.New().String(),
		PostId:    post.Uuid,
		CreatedAt: post.CreatedAt,
		Size:      size,
		FileName:  fileName,
		Extension: extension,
		Index:     index,
	}
	post.HasAttachment = true
	_ = Db.Save(&post)
	err = Db.Save(&attachment)
	return attachment, nil
}

// Remove an attachment from the database and related files.
func DeleteAttachment(uuid string) error {
	attachment, err := GetAttachment(uuid)
	if err != nil {
		return err
	}
	mediasFolder := GetMediaFolder()
	fileName := GetAttachmentFileName(attachment)
	folderName := GetAttachmentFolderName(attachment)
	dstFolder := mediasFolder + "/" + folderName
	path := dstFolder + "/" + fileName
	err = os.Remove(path)
	if err != nil {
		fmt.Println(err)
	}
	err = Db.DeleteStruct(&attachment)
	return err
}

// Save avatar file for given contact.
func SaveAvatar(
	contact Contact,
	file *multipart.FileHeader,
) (Contact, error) {
	folderName := "avatars"
	fileName := contact.Uuid + ".png"
	_, err := saveFileInMedias(file, folderName, fileName)
	if err != nil {
		return Contact{}, err
	}
	contact.HasAvatar = true
	_ = Db.Save(&contact)
	return contact, nil
}

// Retrieve all contacts.
func GetContacts() ([]Contact, error) {
	var contacts []Contact
	err := Db.All(&contacts)
	if err != nil {
		return nil, err
	}
	return contacts, nil
}

// Retrieve given contact.
func GetContact(uuid string) (Contact, error) {
	var contact Contact
	err := Db.One("Uuid", uuid, &contact)
	if err != nil {
		return Contact{}, err
	}
	return contact, nil
}

// Retrieve all for given url.
func GetContactFromUrl(url string) (Contact, error) {
	var contact Contact
	err := Db.One("Url", url, &contact)
	if err != nil {
		return Contact{}, err
	}
	return contact, nil
}

// Create a new contact with given url. Default status is SENT, (means that
// an invite was sent.
func CreateContact(url string) (Contact, error) {
	contact, err := GetContactFromUrl(url)
	if err != nil && err.Error() != "not found" {
		return Contact{}, err
	}
	if err.Error() != "not found" {
		return contact, err
	}
	contact = Contact{
		Uuid:      guuid.New().String(),
		Name:      "",
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
		Url:       url,
		Key:       "",
		LocalKey:  guuid.New().String(),
		Status:    "SENT",
	}
	fmt.Println("contact to create", contact.Url)
	err = Db.Save(&contact)
	if err != nil {
		return Contact{}, err
	}
	return contact, nil
}

// Change the status of a given contact.
func SetContactStatus(uuid string, status string) (Contact, error) {
	contact, err := GetContact(uuid)
	if err != nil && err.Error() != "not found" {
		return Contact{}, err
	}
	contact.Status = status
	err = Db.Save(&contact)
	if err != nil {
		return Contact{}, err
	}
	return contact, nil
}

// Change the key of a given contact.
func SetContactKey(uuid string, key string) (Contact, error) {
	contact, err := GetContact(uuid)
	if err != nil && err.Error() != "not found" {
		return Contact{}, err
	}
	contact.Key = key
	err = Db.Save(&contact)
	if err != nil {
		return Contact{}, err
	}
	return contact, nil
}

// Change the name of a given contact.
func SetContactName(uuid string, name string) (Contact, error) {
	contact, err := GetContact(uuid)
	if err != nil && err.Error() != "not found" {
		return Contact{}, err
	}
	contact.Name = name
	err = Db.Save(&contact)
	if err != nil {
		return Contact{}, err
	}
	return contact, nil
}

// Delete contact matching give uuid.
func DeleteContact(uuid string) error {
	contact, err := GetContact(uuid)
	if err != nil {
		return nil
	}
	if contact.Status == "OK" {
		_, err = SetContactStatus(uuid, "DELETED")
	} else {
		err = Db.DeleteStruct(&contact)
	}
	return err
}

// Get the attachment maching given UUID.
func GetAttachment(uuid string) (Attachment, error) {
	var attachment Attachment
	err := Db.One("Uuid", uuid, &attachment)
	if err != nil {
		return Attachment{}, err
	}
	return attachment, nil
}

// Delete message matching given Id and relate attachments.
func DeleteMessage(uuid string) error {
	attachments, err := GetAttachments([]string{uuid})
	if err != nil {
		attachments = []Attachment{}
	}
	for _, attachment := range attachments {
		err = DeleteAttachment(attachment.Uuid)
		if err != nil {
			fmt.Println("Attachment deletion failed: " + attachment.Uuid)
		}
	}
	message, err := GetPost(uuid)
	if err != nil {
		return nil
	}
	err = Db.DeleteStruct(&message)
	return err
}
