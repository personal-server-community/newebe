package database

import (
	"os"

	"github.com/asdine/storm"
)

var Db = GetDB()

// Get database instance. Database path can be given throug DB_NAME environment
// variable.
func GetDB() *storm.DB {
	dbName, empty := os.LookupEnv("DB_NAME")
	if !empty {
		dbName = "newebe.db"
	}
	db, err := storm.Open(dbName)
	if err != nil {
		panic(err)
	}
	return db
}
