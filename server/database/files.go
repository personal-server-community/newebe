package database

import (
	"bytes"
	"fmt"
	"image/jpeg"
	"image/png"
	"net/http"
	"os"
	"strconv"
)

// Utility function to retrieve the folder storing medias. It can be set
// through MEDIAS_FOLDER, where ./medias is the default folder.
func GetMediaFolder() string {
	mediasFolder, empty := os.LookupEnv("MEDIAS_FOLDER")
	if !empty {
		mediasFolder = "medias"
	}
	return mediasFolder
}

// Utility function to build the subfolder name to store an attachment.
func GetAttachmentFolderName(attachment Attachment) string {
	return attachment.CreatedAt.Format(LAYOUT_DATE)
}

// Utility function to build the file name to store an attachment.
func GetAttachmentFileName(attachment Attachment) string {
	date := attachment.CreatedAt.Format(LAYOUT_DATETIME)
	return date + "-" + attachment.PostId + "-" +
		strconv.Itoa(attachment.Index) + "." + attachment.Extension
}

func GetAttachmentPath(attachment Attachment) string {
	mediasFolder := GetMediaFolder()
	fileName := GetAttachmentFileName(attachment)
	folderName := GetAttachmentFolderName(attachment)
	attachmentFolder := mediasFolder + "/" + folderName
	attachmentPath := attachmentFolder + "/" + fileName
	return attachmentPath
}

// Get the avatar path of current user.
func GetUserAvatarPath() string {
	fileName := "avatar.png"
	mediasFolder := GetMediaFolder()
	avatarFolder := mediasFolder + "/"
	avatarPath := avatarFolder + "/" + fileName
	return avatarPath
}

// ToPng converts an image to png
func ToPng(imageBytes []byte) ([]byte, error) {
	contentType := http.DetectContentType(imageBytes)

	switch contentType {
	case "image/png":
	case "image/jpeg":
		img, err := jpeg.Decode(bytes.NewReader(imageBytes))
		if err != nil {
			return nil, err
		}

		buf := new(bytes.Buffer)
		if err := png.Encode(buf, img); err != nil {
			return nil, err
		}

		return buf.Bytes(), nil
	}

	return nil, fmt.Errorf("unable to convert %#v to png", contentType)
}
