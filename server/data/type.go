package data

import (
	"time"
)

type (
	Message struct { // Describe a messag to post
		Content       string    `json:"content" validate:"required"`
		Uuid          string    `json:"uuid"`
		AuthorKey     string    `json:"authorKey"`
		CreatedAt     time.Time `json:"createdAt"`
		HasAttachment bool      `json:"hasAttachment"`
	}

	PbMessage struct { // Describe a message sent to the pubsub websocket
		Content       string    `json:"content"`
		Uuid          string    `json:"uuid"`
		AuthorKey     string    `json:"authorKey"`
		CreatedAt     time.Time `json:"createdAt"`
		HasAttachment bool      `json:"hasAttachment"`
		EventType     string    `json:"eventType"`
	}

	PbAttachment struct { // Describe an attachment sent to the pubsub websocket
		Uuid      string    `json:"uuid"`
		CreatedAt time.Time `json:"createdAt"`
		PostId    string    `json:"postId"`
		Size      int64     `json:"size"`
		FileName  string    `json:"fileName"`
		Extension string    `json:"extension"`
		Index     int       `json:"index"`
		EventType string    `json:"eventType"`
	}

	// Describe a message deletion request sent to the pubsub websocket.
	PbMessageDeletion struct {
		Uuid      string `json:"uuid"`
		EventType string `json:"eventType"`
	}

	PbContact struct { // Describe contact information sent to the pubsub websocket.
		Name      string `json:"name"`
		Uuid      string `json:"uuid"`
		Status    string `json:"status"`
		Url       string `json:"url"`
		EventType string `json:"eventType"`
	}

	PbContactStatus struct { // Describe a contact status change sent to the pubsub websocket.
		Name      string `json:"name"`
		Uuid      string `json:"uuid"`
		Status    string `json:"status"`
		EventType string `json:"eventType"`
	}

	// Describe a contact deletion request sent to the pubsub websocket.
	PbContactDelete struct {
		Uuid      string `json:"uuid"`
		EventType string `json:"eventType"`
	}

	Profile struct { // Describe the user profile
		Uuid      string `json:"uuid"`
		Key       string `json:"key"`
		Name      string `json:"name" validate:"required"`
		HasAvatar bool   `json:"hasAvatar"`
		Url       string `json:"url" validate:"required"`
	}

	RegisterInfo struct { // Describe the info required to register.
		Name     string `json:"name" validate:"required"`
		Url      string `json:"url" validate:"required"`
		Password []byte `json:"password" validate:"required"`
	}

	Status struct { // Describe the user status change
		Registered bool `json:"registered"`
	}

	Authenticated struct { // Desribe the current authentication status
		Authenticated bool `json:"authenticated"`
	}

	Credentials struct { // Describe login data
		Password []byte `json:"password"`
	}

	ContactInfo struct { // Describe a contact
		Url       string    `json:"url" validate:"required"`
		Uuid      string    `json:"uuid"`
		CreatedAt time.Time `json:"createdAt"`
		Name      string    `json:"name"`
		Status    string    `json:"status"`
		Key       string    `json:"key"`
		LocalKey  string    `json:"localKey"`
		HasAvatar bool      `json:"hasAvatar"`
	}

	ContactMessage struct { // Describe a message received by a contact
		Content  string `json:"content" validate:"required"`
		Key      string `json:"key" validate:"required"`
		LocalKey string `json:"localKey" validate:"required"`
	}

	PeerContactInfo struct { // Contact data sent to other peers
		Name     string `json:"name"`
		Url      string `json:"url"`
		Key      string `json:"key"`
		LocalKey string `json:"localKey"`
	}

	PeerContactMessage struct { // Message data sent to other peers
		Content       string `json:"content" validate:"required"`
		Uuid          string `json:"uuid" validate:"required"`
		CreatedAt     string `json:"createdAt" validate:"required"`
		HasAttachment bool   `json:"hasAttachment"`
		Key           string `json:"key" validate:"required"`
		LocalKey      string `json:"localKey" validate:"required"`
	}

	PeerContactAttachment struct { // Attachment data sent to other peers
		Uuid      string `json:"uuid" validate:"required"`
		CreatedAt string `json:"createdAt" validate:"required"`
		PostId    string `json:"postId" validate: required`
		Size      int64  `json:"size"`
		FileName  string `json:"fileName"`
		Key       string `json:"key" validate:"required"`
		LocalKey  string `json:"localKey" validate:"required"`
		Extension string `json:"extension" validate:"required"`
		Index     int    `json:"index" validate:"required"`
	}

	PeerContactAvatar struct { // Avatar info sent to other peer
		Uuid     string `json:"uuid" validate:"required"`
		Key      string `json:"key" validate:"required"`
		LocalKey string `json:"localKey" validate:"required"`
	}

	AuthToken struct {
		Uuid       string `json:"uuid"`
		CreatedAt  string `json:"createdAt"`
		Token      string `json:"token"`
		Expiration string `json:"expiration"`
	}
)
