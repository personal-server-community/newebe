package peers

// Bunch of helpers to communicate with user contacts (peers). All
// communications are based on HTTP requests.
// The local key and the official contact key is given to allow the remote
// peer to authenticate the user.

import (
	"fmt"
	"time"

	"github.com/parnurzeal/gorequest"
	"gitlab.com/personal-server-community/newebe/server/data"
	"gitlab.com/personal-server-community/newebe/server/database"
)

// Helper fuction to post data to a given server.
func httpPost(
	url string,
	data interface{},
) (gorequest.Response, string, []error) {
	request := gorequest.New()
	return request.Post(url).
		Send(data).
		End()
}

// Helper fuction to post data with files to a given server.
func httpPostWithFile(
	url string,
	data interface{},
	filePath string,
) (gorequest.Response, string, []error) {
	request := gorequest.New()
	return request.Post(url).
		Type("multipart").
		SetDebug(true).
		Send(data).
		SendFile(filePath, "", "file").
		End()
}

// Transmit a message to all contacts.
func SendMessageToContacts(message database.Post) error {
	user, err := database.GetUser()
	if err != nil {
		return err
	}
	contacts, err := database.GetContacts()
	if err != nil {
		return err
	}
	for _, contact := range contacts {
		if contact.Status != "DELETED" {
			_ = SendMessageToContact(user, contact, message)
		}
	}
	return nil
}

// Share a message with a given contact.
func SendMessageToContact(
	user database.User,
	contact database.Contact,
	message database.Post,
) error {
	infos := data.PeerContactMessage{
		Uuid:          message.Uuid,
		Content:       message.Content,
		CreatedAt:     message.CreatedAt.Format(time.RFC3339),
		Key:           contact.LocalKey,
		LocalKey:      contact.Key,
		HasAttachment: false,
	}
	url := contact.Url + "/api/incoming/messages/new"
	_, _, errs := httpPost(url, infos)
	// fmt.Println(resp)
	// fmt.Println(body)
	// fmt.Println(errs)
	if len(errs) > 0 {
		return errs[0]
	}
	return nil
}

// Share a message deletion to all contacts.
func SendMessageDeletionToContacts(
	message database.Post,
) error {
	user, err := database.GetUser()
	if err != nil {
		return err
	}
	contacts, err := database.GetContacts()
	if err != nil {
		return err
	}
	for _, contact := range contacts {
		if contact.Status != "DELETED" {
			_ = SendMessageDeletionToContact(user, contact, message)
		}
	}
	return nil
}

// Share a message deletion to a given contact.
func SendMessageDeletionToContact(
	user database.User,
	contact database.Contact,
	message database.Post,
) error {
	infos := data.PeerContactMessage{
		Uuid:          message.Uuid,
		Content:       message.Content,
		CreatedAt:     message.CreatedAt.Format(time.RFC3339),
		Key:           contact.LocalKey,
		LocalKey:      contact.Key,
		HasAttachment: false,
	}
	url := contact.Url + "/api/incoming/messages/deletion"
	_, _, errs := httpPost(url, infos)
	// fmt.Println(resp)
	// fmt.Println(body)
	// fmt.Println(errs)
	if len(errs) > 0 {
		return errs[0]
	}
	return nil
}

// Share an attachment to all contacts.
func SendAttachmentToContacts(
	message database.Post,
	attachment database.Attachment,
) error {
	user, err := database.GetUser()
	if err != nil {
		return err
	}
	contacts, err := database.GetContacts()
	if err != nil {
		return err
	}
	for _, contact := range contacts {
		if contact.Status != "DELETED" {
			_ = SendAttachmentToContact(user, contact, message, attachment)
		}
	}
	return nil
}

// Share an attachment to a given contact.
func SendAttachmentToContact(
	user database.User,
	contact database.Contact,
	message database.Post,
	attachment database.Attachment,
) error {
	fmt.Println(attachment.Index)
	infos := data.PeerContactAttachment{
		Uuid:      attachment.Uuid,
		PostId:    attachment.PostId,
		Size:      attachment.Size,
		FileName:  attachment.FileName,
		Extension: attachment.Extension,
		Index:     attachment.Index,
		CreatedAt: attachment.CreatedAt.Format(time.RFC3339),
		Key:       contact.LocalKey,
		LocalKey:  contact.Key,
	}
	attachmentPath := database.GetAttachmentPath(attachment)
	url := contact.Url + "/api/incoming/attachments/new"
	_, _, errs := httpPostWithFile(url, infos, attachmentPath)
	// fmt.Println(url, dstPath)
	if len(errs) > 0 {
		fmt.Println(attachment.Index)
		return errs[0]
	}
	return nil
}

// Send the user avatar to all contacts.
func SendAvatarToContacts() error {
	user, err := database.GetUser()
	if err != nil {
		return err
	}
	contacts, err := database.GetContacts()
	if err != nil {
		return err
	}
	for _, contact := range contacts {
		if contact.Status != "DELETED" {
			_ = SendAvatarToContact(user, contact)
		}
	}
	return nil
}

// Send an avatar to a given contact.
func SendAvatarToContact(
	user database.User,
	contact database.Contact,
) error {
	infos := data.PeerContactAvatar{
		Uuid:     user.Uuid,
		Key:      contact.LocalKey,
		LocalKey: contact.Key,
	}
	avatarPath := database.GetUserAvatarPath()
	url := contact.Url + "/api/incoming/avatar"
	_, _, errs := httpPostWithFile(url, infos, avatarPath)
	if len(errs) > 0 {
		return errs[0]
	}
	return nil
}

// Send a contact request to given URL.
func SendContactRequest(
	user database.User,
	contact database.Contact,
) error {
	infos := data.PeerContactInfo{
		Name:     user.Name,
		Url:      user.Url,
		Key:      contact.LocalKey,
		LocalKey: "",
	}
	url := contact.Url + "/api/incoming/contacts/request"
	_, _, errs := httpPost(url, infos)
	if len(errs) > 0 {
		return errs[0]
	}
	return nil
}

// Send a confirmation approval to a given contact.
func SendConfirmContact(
	user database.User,
	contact database.Contact,
) error {
	infos := data.PeerContactInfo{
		Name: user.Name,
		Url:  user.Url,
		Key:  contact.LocalKey,
	}
	_, _, errs := httpPost(contact.Url+"/api/incoming/contacts/confirm", infos)
	if len(errs) > 0 {
		return errs[0]
	}
	return nil
}

// Send a revokation to a given contact.
func SendRevokeContact(
	user database.User,
	contact database.Contact,
) error {
	infos := data.PeerContactInfo{
		Name: user.Name,
		Url:  user.Url,
		Key:  user.Key,
	}
	_, _, errs := httpPost(contact.Url+"/api/incoming/contacts/revoke", infos)
	if len(errs) > 0 {
		return errs[0]
	}
	return nil
}
