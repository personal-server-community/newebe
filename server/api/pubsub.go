package api

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"github.com/labstack/echo/v4"
	"gitlab.com/personal-server-community/newebe/server/data"
	"gitlab.com/personal-server-community/newebe/server/database"
)

// Series of struct to store the connected clients list and
type (

	// Pubsub client list
	PbClients struct {
		Clients []PbClient
	}

	// Describe a connected client
	PbClient struct {
		Id   string
		Conn *websocket.Conn
	}

	// Describe a message to send peers
	PbMessage struct {
		Action  string          `json:"action"`
		Topic   string          `json:"topic"`
		Message json.RawMessage `json:"message"`
	}
)

// Registry listing all connected websocket clients.
var pubSubRegistry = &PbClients{}

var (
	upgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}
)

// Send given data encapsulated in the pubsub message structure to all clients
// connected.
func publish(data interface{}) error {
	text, err := json.Marshal(data)
	if err != nil {
		return err
	}
	for _, client := range pubSubRegistry.Clients {
		fmt.Println("write to client : ", client.Id)
		err = client.Conn.WriteMessage(
			websocket.TextMessage,
			text,
		)
		if err != nil {
			fmt.Println(err)
		}
	}
	return nil
}

// Helper to send a newebe message data to all clients through the pub/sub
// mechanism. It is useful to updates UI in realtime.
func publishMessage(c echo.Context, post database.Post) error {
	infos := data.PbMessage{
		Content:       post.Content,
		CreatedAt:     post.CreatedAt,
		Uuid:          post.Uuid,
		HasAttachment: post.HasAttachment,
		AuthorKey:     post.AuthorKey,
		EventType:     "message:new",
	}
	return publish(infos)
}

// Helper to send a newebe attachment data to all clients through the pub/sub
// mechanism. It is useful to updates UI in realtime.
func publishAttachment(c echo.Context, attachment database.Attachment) error {
	infos := data.PbAttachment{
		Uuid:      attachment.Uuid,
		CreatedAt: attachment.CreatedAt,
		PostId:    attachment.PostId,
		FileName:  attachment.FileName,
		Extension: attachment.Extension,
		Index:     attachment.Index,
		Size:      attachment.Size,
		EventType: "attachment:new",
	}
	return publish(infos)
}

// Helper to send a newebe message deletion info to all clients through the
// pub/sub mechanism. It is useful to updates UI in realtime.
func publishMessageDeletion(c echo.Context, post database.Post) error {
	infos := data.PbMessageDeletion{
		Uuid:      post.Uuid,
		EventType: "message:delete",
	}
	return publish(infos)
}

// Helper to send a newebe contact data to all clients through the pub/sub
// mechanism. It is useful to updates UI in realtime.
func publishCreateContact(c echo.Context, contact database.Contact) error {
	infos := data.PbContact{
		Name:      contact.Name,
		Url:       contact.Url,
		Uuid:      contact.Uuid,
		Status:    contact.Status,
		EventType: "contact:new",
	}
	return publish(infos)
}

// Helper to send a newebe contact status change to all clients through the
// pub/sub mechanism. It is useful to updates UI in realtime.
func publishContactStatus(c echo.Context, contact database.Contact) error {
	infos := data.PbContactStatus{
		Uuid:      contact.Uuid,
		Name:      contact.Name,
		Status:    contact.Status,
		EventType: "contact:status-change",
	}
	return publish(infos)
}

// Helper to send a newebe contact deletion to all clients through the
// pub/sub mechanism. It is useful to updates UI in realtime.
func publishDeleteContact(c echo.Context, contact database.Contact) error {
	infos := data.PbContactDelete{
		Uuid:      contact.Uuid,
		EventType: "contact:delete",
	}
	return publish(infos)
}

// Handle websocket connections. Add the connected client to the registry
// Handle incoming messages. If an error occurs, the client is disconnected
// and is removed from the registry
func pubsub(c echo.Context) error {
	conn, err := upgrader.Upgrade(c.Response(), c.Request(), nil)
	if err != nil {
		return err
	}
	defer conn.Close()

	id := uuid.New().String()
	client := PbClient{Id: id, Conn: conn}
	pubSubRegistry.Clients = append(pubSubRegistry.Clients, client)
	fmt.Println("New ws client is connected: ", id)
	c.Logger().Info("New ws client is connected: " + id)

	for {
		_, _, err := conn.ReadMessage()
		if err != nil {
			pubSubRegistry.Clients = removeClientFromRegistry(
				pubSubRegistry.Clients,
				id,
			)
			c.Logger().Info("WS Client is disconnected: " + id)
			fmt.Println("WS Client is disconnected: ", id)
			break
		}
	}
	return nil
}

// Helper to remove a client from in the registry.
func removeClientFromRegistry(clients []PbClient, id string) []PbClient {
	index := findClientIndex(clients, id)
	if index > 0 {
		return append(
			clients[:index],
			clients[index+1:]...,
		)
	}
	return clients
}

// Helper to retrieve a client position in the registry.
func findClientIndex(clients []PbClient, id string) int {
	for i, client := range clients {
		if client.Id == id {
			return i
		}
	}
	return -1
}
