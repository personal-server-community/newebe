package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"image"
	"image/png"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/disintegration/imaging"
	"github.com/go-errors/errors"
	"github.com/labstack/echo/v4"
	"gitlab.com/personal-server-community/newebe/server/data"
	"gitlab.com/personal-server-community/newebe/server/database"
	"gitlab.com/personal-server-community/newebe/server/peers"
	"golang.org/x/crypto/bcrypt"
)

const LAYOUT_DATETIME = "2006-01-02-15:04:05"

type (
	MessageListResult struct {
		Messages    []data.Message        `json:"messages"`
		Attachments []database.Attachment `json:"attachments"`
	}
)

func handleError(c echo.Context, err error) error {
	text := errors.Wrap(err, 2).ErrorStack()
	fmt.Println(text)
	c.Logger().Error(err.Error())
	return c.JSON(
		http.StatusInternalServerError,
		Error{
			Name: "Internal Server Error",
			Text: err.Error(),
		},
	)
}

func handleParameterError(c echo.Context, err error) error {
	text := errors.Wrap(err, 2).ErrorStack()
	fmt.Println(text)
	c.Logger().Error(err.Error())
	return c.JSON(
		http.StatusBadRequest,
		Error{
			Name: "Wrong parameters",
			Text: err.Error(),
		},
	)
}

func handleParameterTextError(c echo.Context, text string) error {
	fmt.Println(text)
	c.Logger().Error(text)
	return c.JSON(
		http.StatusBadRequest,
		Error{
			Name: "Wrong parameters",
			Text: text,
		},
	)
}

func ConvertBodyContent(c echo.Context) echo.Context {
	var bodyBytes []byte
	if c.Request().Body != nil {
		bodyBytes, _ = ioutil.ReadAll(c.Request().Body)
	}
	c.Request().Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
	// fmt.Println("ConvertBodyContent", c.Request().Body)
	return c
}

func StorePictureFileFromBody(
	c echo.Context,
	path string,
) (*os.File, image.Image, error) {
	file, err := c.FormFile("file")
	if err != nil {
		return nil, nil, err
	}
	buf, _ := file.Open()
	src := bytes.NewBuffer(nil)
	if _, err := io.Copy(src, buf); err != nil {
		return nil, nil, err
	}
	srcbytes := src.Bytes()
	contentType := http.DetectContentType(srcbytes)
	if contentType == "image/jpeg" {
		srcbytes, _ = database.ToPng(srcbytes)
	}
	srcImage, err := imaging.Decode(
		bytes.NewReader(srcbytes),
		imaging.AutoOrientation(true),
	)
	dst, err := os.Create(path)
	if err != nil {
		return nil, nil, err
	}
	defer dst.Close()
	png.Encode(dst, srcImage)
	return dst, srcImage, nil
}

func StoreThumbnail(dst *os.File, srcImage image.Image, path string) error {
	png.Encode(dst, srcImage)
	dstTh, err := os.Create(path)
	if err != nil {
		return err
	}
	defer dstTh.Close()
	thumbnail := imaging.Thumbnail(srcImage, 150, 150, imaging.Lanczos)
	if err != nil {
		return err
	}
	png.Encode(dstTh, thumbnail)
	return nil
}

// Return a simple JSON structure givin the version of the server.
func version(c echo.Context) error {
	data := Version{
		Number:   VERSION,
		Software: "Newebe",
	}
	return c.JSON(http.StatusOK, data)
}

func getUser(c echo.Context) error {
	user, err := database.GetUser()
	if err != nil {
		return handleParameterTextError(
			c, "Cannot Find User, please register to your Newebe")
	}
	return c.JSON(http.StatusOK, data.Profile{
		Name:      user.Name,
		Url:       user.Url,
		Key:       user.Key,
		HasAvatar: user.HasAvatar,
		Uuid:      user.Uuid,
	})
}

func updateUser(c echo.Context) error {
	ConvertBodyContent(c)
	p := new(data.Profile)
	if err := c.Bind(p); err != nil {
		return handleParameterError(c, err)
	}
	if err := c.Validate(p); err != nil {
		return handleParameterError(c, err)
	}
	profile, err := database.UpdateUser(*p)
	if err != nil {
		return handleError(c, err)
	}
	return c.JSON(http.StatusOK, profile)
}

func setProfilePicture(c echo.Context) error {
	avatarPath := database.GetUserAvatarPath()
	dst, srcImage, err := StorePictureFileFromBody(c, avatarPath)
	if err != nil {
		return handleError(c, err)
	}
	thPath := database.GetMediaFolder() + "/th-avatar.png"
	err = StoreThumbnail(dst, srcImage, thPath)
	if err != nil {
		return handleError(c, err)
	}
	user, err := database.GetUser()
	if err != nil {
		return handleParameterError(c, err)
	}
	err = database.SetHasAvatar(user)
	if err != nil {
		return handleError(c, err)
	}
	peers.SendAvatarToContacts()
	return c.JSON(http.StatusOK, "")
}

func GetIntQueryParam(c echo.Context, param string, defaultValue int) (int64, error) {
	paramValue, err := strconv.ParseInt(c.QueryParam(param), defaultValue, 0)
	return paramValue, err
}

func getMessages(c echo.Context) error {
	var postIds []string
	var messages []data.Message
	page, _ := GetIntQueryParam(c, "page", 10)
	posts, err := database.GetPosts(int(page))
	if err != nil && err.Error() != "not found" {
		return handleError(c, err)
	}
	for _, post := range posts {
		postIds = append(postIds, post.Uuid)
		messages = append(messages, data.Message{
			Content:       post.Content,
			CreatedAt:     post.CreatedAt,
			AuthorKey:     post.AuthorKey,
			Uuid:          post.Uuid,
			HasAttachment: post.HasAttachment,
		})
	}
	attachments, err := database.GetAttachments(postIds)
	if err != nil && err.Error() != "not found" {
		return handleError(c, err)
	}
	result := MessageListResult{
		Attachments: attachments,
		Messages:    messages,
	}
	return c.JSON(http.StatusOK, result)
}

func createMessage(c echo.Context) error {
	ConvertBodyContent(c)
	m := new(data.Message)
	if err := c.Bind(m); err != nil {
		return handleParameterError(c, err)
	}
	if err := c.Validate(m); err != nil {
		return handleParameterError(c, err)
	}
	post, err := database.CreatePost(m.Content, false)
	if err != nil {
		return err
	}
	publishMessage(c, post)
	peers.SendMessageToContacts(post)
	return c.JSON(http.StatusCreated, post)
}

func createPostWithAttachments(c echo.Context) error {
	content := c.FormValue("content")
	post, err := database.CreatePost(content, true)
	if err != nil {
		return handleError(c, err)
	}

	var attachments []database.Attachment
	form, err := c.MultipartForm()
	index := 1
	for _, tmp := range form.File {
		for _, file := range tmp {
			attachment, err := database.SaveAttachment(post, index, file)
			if err != nil {
				return handleError(c, err)
			}
			attachments = append(attachments, attachment)
		}
		index = index + 1
	}

	publishMessage(c, post)
	peers.SendMessageToContacts(post)
	for _, attachment := range attachments {
		publishAttachment(c, attachment)
		peers.SendAttachmentToContacts(post, attachment)
	}
	postJson, err := json.Marshal(post)
	return c.JSON(http.StatusCreated, postJson)
}

func getAttachments(c echo.Context) error {
	attachments, err := database.GetLastAttachments()
	if err != nil && err.Error() != "not found" {
		return handleError(c, err)
	}
	return c.JSON(http.StatusOK, attachments)
}

func deleteMessage(c echo.Context) error {
	id := c.Param("id")
	message, err := database.GetPost(id)
	if err != nil && err.Error() != "not found" {
		return handleError(c, err)
	}
	err = database.DeleteMessage(id)
	if err != nil && err.Error() != "not found" {
		return handleError(c, err)
	}
	publishMessageDeletion(c, message)
	peers.SendMessageDeletionToContacts(message)
	return c.JSON(http.StatusNoContent, "")
}

func login(c echo.Context) error {
	ConvertBodyContent(c)
	credentials := new(data.Credentials)
	if err := c.Bind(credentials); err != nil {
		handleParameterError(c, err)
	}
	password := credentials.Password
	user, err := database.GetUser()
	if err != nil {
		return handleError(c, err)
	}
	err = bcrypt.CompareHashAndPassword(user.Password, []byte(password))
	if err != nil {
		text := errors.Wrap(err, 2).ErrorStack()
		fmt.Println(text)
		fmt.Println(err.Error())
		fmt.Println(password)
		c.Logger().Error(err.Error())
		c.Logger().Error(password)
		c.Logger().Error(user.Password)
		return echo.ErrUnauthorized
	}

	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["name"] = user.Name
	claims["exp"] = time.Now().Add(time.Hour * 24 * 7).Unix()

	secret, empty := os.LookupEnv("AUTH_JWT_SECRET")
	if !empty {
		return handleError(c, errors.New("Auth secret not configured"))
	}
	t, err := token.SignedString([]byte(secret))
	if err != nil {
		return err
	}
	_, err = database.CreateAuthToken(t)
	if err != nil {
		return handleError(c, err)
	}
	return c.JSON(http.StatusOK, map[string]string{
		"token": t,
	})
}

func logout(c echo.Context) error {
	t := c.Request().Header.Get("Authorization")[7:]
	err := database.DeleteAuthToken(t)
	if err != nil {
		return handleParameterError(c, errors.New("No token to remove."))
	} else {
		return c.JSON(http.StatusNoContent, "")
	}
}

func register(c echo.Context) error {
	user, err := database.GetUser()
	if err == nil {
		return c.JSON(http.StatusBadRequest, Error{
			Name: "Wrong parameters",
			Text: "User already exists",
		})
	}
	ConvertBodyContent(c)
	info := new(data.RegisterInfo)
	if err := c.Bind(info); err != nil {
		handleParameterError(c, err)
	}
	hashedPassword, _ := bcrypt.GenerateFromPassword(
		info.Password,
		bcrypt.DefaultCost,
	)
	user, err = database.CreateUser(
		info.Name,
		info.Url,
		hashedPassword,
	)
	if err != nil {
		return handleError(c, err)
	}
	return c.JSON(http.StatusCreated, data.Profile{
		Name:      user.Name,
		Url:       user.Url,
		HasAvatar: user.HasAvatar,
	})
}

func status(c echo.Context) error {
	_, err := database.GetUser()
	return c.JSON(http.StatusOK, data.Status{
		Registered: err == nil,
	})
}

func authenticated(c echo.Context) error {
	return c.JSON(http.StatusOK, data.Authenticated{
		Authenticated: true,
	})
}

func ConvertContact(contact database.Contact) data.ContactInfo {
	return data.ContactInfo{
		Uuid:      contact.Uuid,
		Url:       contact.Url,
		CreatedAt: contact.CreatedAt,
		Name:      contact.Name,
		Status:    contact.Status,
		Key:       contact.Key,
		LocalKey:  contact.LocalKey,
		HasAvatar: contact.HasAvatar,
	}
}

func ConvertToken(token database.AuthToken) data.AuthToken {
	return data.AuthToken{
		Uuid:       token.Uuid,
		CreatedAt:  token.CreatedAt.Format(LAYOUT_DATETIME),
		Token:      token.Token,
		Expiration: token.Expiration.Format(LAYOUT_DATETIME),
	}
}

func getContact(c echo.Context) error {
	id := c.Param("id")
	contact, err := database.GetContact(id)
	if err != nil {
		return handleError(c, err)
	}
	return c.JSON(http.StatusOK, ConvertContact(contact))
}

func getContacts(c echo.Context) error {
	contacts, err := database.GetContacts()
	var contactInfos []data.ContactInfo
	if err != nil && err.Error() != "not found" {
		return handleError(c, err)
	}
	for _, contact := range contacts {
		contactInfos = append(contactInfos, ConvertContact(contact))
	}
	return c.JSON(http.StatusOK, contactInfos)
}

func getTokens(c echo.Context) error {
	tokens, err := database.GetAuthTokens()
	var tokenInfos []data.AuthToken
	if err != nil && err.Error() != "not found" {
		return handleError(c, err)
	}
	for _, token := range tokens {
		tokenInfos = append(tokenInfos, ConvertToken(token))
	}
	return c.JSON(http.StatusOK, tokenInfos)
}

func createContact(c echo.Context) error {
	ConvertBodyContent(c)
	info := new(data.ContactInfo)
	if err := c.Bind(info); err != nil {
		return handleParameterError(c, err)
	}
	if err := c.Validate(info); err != nil {
		return handleParameterError(c, err)
	}
	url := strings.Trim(info.Url, " ")
	if !strings.HasPrefix(url, "https://") &&
		!strings.HasPrefix(url, "http://") {
		url = "https://" + url
	}
	contact, err := database.CreateContact(url)
	if err != nil {
		return handleParameterError(c, err)
	}
	user, err := database.GetUser()
	if err != nil {
		return handleParameterError(c, err)
	}
	publishCreateContact(c, contact)
	err = peers.SendContactRequest(user, contact)
	if err != nil {
		contact, err := database.SetContactStatus(contact.Uuid, "ERROR")
		if err != nil {
			return handleParameterError(c, err)
		}
		publishContactStatus(c, contact)
	}
	return c.JSON(http.StatusOK, ConvertContact(contact))
}

func deleteContact(c echo.Context) error {
	id := c.Param("id")
	contact, err := database.GetContact(id)
	if err != nil {
		return handleError(c, err)
	}
	err = database.DeleteContact(id)
	if err != nil {
		return handleError(c, err)
	}
	publishDeleteContact(c, contact)
	user, err := database.GetUser()
	if err != nil {
		return handleParameterError(c, err)
	}
	_ = peers.SendRevokeContact(user, contact)
	return c.JSON(http.StatusNoContent, "")
}

func requestContact(c echo.Context) error {
	ConvertBodyContent(c)
	info := new(data.PeerContactInfo)
	if err := c.Bind(info); err != nil {
		return handleParameterError(c, err)
	}
	contact, err := database.CreateContact(info.Url)
	if err != nil {
		return handleParameterError(c, err)
	}
	contact, err = database.SetContactName(contact.Uuid, info.Name)
	if err != nil {
		return handleError(c, err)
	}
	contact, err = database.SetContactStatus(contact.Uuid, "PENDING")
	contact, err = database.SetContactKey(contact.Uuid, info.Key)
	if err != nil {
		return handleError(c, err)
	}
	publishCreateContact(c, contact)
	return c.JSON(http.StatusOK, "")
}

func confirmContact(c echo.Context) error {
	id := c.Param("id")
	contact, err := database.SetContactStatus(id, "OK")
	if err != nil {
		return handleError(c, err)
	}
	user, err := database.GetUser()
	if err != nil {
		return handleError(c, err)
	}
	err = peers.SendConfirmContact(user, contact)
	if err != nil {
		_, err := database.SetContactStatus(id, "ERROR")
		if err != nil {
			return handleError(c, err)
		}
		publishContactStatus(c, contact)
		return c.JSON(http.StatusOK, "")
	}
	peers.SendAvatarToContact(user, contact)
	publishContactStatus(c, contact)
	return c.JSON(http.StatusOK, "")
}

func confirmedContact(c echo.Context) error {
	ConvertBodyContent(c)
	info := new(data.PeerContactInfo)
	if err := c.Bind(info); err != nil {
		return handleParameterError(c, err)
	}
	contact, err := database.GetContactFromUrl(info.Url)
	if err != nil {
		return handleError(c, err)
	}
	contact, err = database.SetContactName(contact.Uuid, info.Name)
	if err != nil {
		return handleError(c, err)
	}
	contact, err = database.SetContactStatus(contact.Uuid, "OK")
	if err != nil {
		return handleError(c, err)
	}
	contact, err = database.SetContactKey(contact.Uuid, info.Key)
	if err != nil {
		return handleError(c, err)
	}
	user, err := database.GetUser()
	if err != nil {
		return handleError(c, err)
	}
	peers.SendAvatarToContact(user, contact)
	publishContactStatus(c, contact)
	return c.JSON(http.StatusOK, "")
}

func revokedContact(c echo.Context) error {
	ConvertBodyContent(c)
	info := new(data.PeerContactInfo)
	if err := c.Bind(info); err != nil {
		return handleParameterError(c, err)
	}
	contact, err := database.GetContactFromUrl(info.Url)
	if err != nil {
		return handleError(c, err)
	}
	contact, err = database.SetContactStatus(contact.Uuid, "REVOKED")
	if err != nil {
		return handleError(c, err)
	}
	publishContactStatus(c, contact)
	return c.JSON(http.StatusOK, "")
}

func revokeContact(c echo.Context) error {
	id := c.Param("id")
	contact, err := database.GetContact(id)
	if err != nil {
		return handleError(c, err)
	}
	contact, err = database.SetContactStatus(id, "REVOKED")
	if err != nil {
		return handleError(c, err)
	}
	publishContactStatus(c, contact)
	user, err := database.GetUser()
	if err != nil {
		return handleError(c, err)
	}
	err = peers.SendRevokeContact(user, contact)
	if err != nil {
		_, err := database.SetContactStatus(contact.Uuid, "ERROR")
		if err != nil {
			return handleParameterError(c, err)
		}
		publishContactStatus(c, contact)
	}
	return c.JSON(http.StatusOK, "")
}

func newIncomingMessage(c echo.Context) error {
	ConvertBodyContent(c)
	m := new(data.PeerContactMessage)
	if err := c.Bind(m); err != nil {
		return handleParameterError(c, err)
	}
	if err := c.Validate(m); err != nil {
		return handleParameterError(c, err)
	}
	if _, err := database.CheckContact(m.Key, m.LocalKey); err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusForbidden, "")
	}
	post, err := database.CreateContactPost(*m)
	if err != nil {
		return handleError(c, err)
	}
	publishMessage(c, post)
	return c.JSON(http.StatusCreated, post)
}

func newIncomingMessageWithAttachments(c echo.Context) error {
	return nil
}

func newIncomingAttachment(c echo.Context) error {
	ConvertBodyContent(c)
	file, err := c.FormFile("file1")
	if err != nil {
		return handleError(c, err)
	}
	size, _ := strconv.ParseInt(c.FormValue("size"), 10, 0)
	index, _ := strconv.Atoi(c.FormValue("index"))
	attachment := data.PeerContactAttachment{
		Uuid:      c.FormValue("uuid"),
		PostId:    c.FormValue("postId"),
		Size:      size,
		FileName:  c.FormValue("fileName"),
		CreatedAt: c.FormValue("createdAt"),
		Key:       c.FormValue("key"),
		LocalKey:  c.FormValue("localKey"),
		Extension: c.FormValue("extension"),
		Index:     index,
	}
	if err := c.Validate(attachment); err != nil {
		return handleParameterError(c, err)
	}
	post, err := database.GetPost(attachment.PostId)
	if err != nil {
		return handleParameterError(c, err)
	}
	_, err = database.CheckContact(attachment.Key, attachment.LocalKey)
	if err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusForbidden, "")
	}
	attachmentInstance, err := database.SaveAttachment(
		post, attachment.Index, file,
	)
	if err != nil {
		return handleError(c, err)
	}
	publishAttachment(c, attachmentInstance)
	attachmentJson, err := json.Marshal(attachmentInstance)
	return c.JSON(http.StatusCreated, attachmentJson)
}

func newIncomingAvatar(c echo.Context) error {
	ConvertBodyContent(c)
	file, err := c.FormFile("file1")
	if err != nil {
		return handleError(c, err)
	}
	avatar := data.PeerContactAvatar{
		Uuid:     c.FormValue("uuid"),
		Key:      c.FormValue("key"),
		LocalKey: c.FormValue("localKey"),
	}
	if err := c.Validate(avatar); err != nil {
		return handleParameterError(c, err)
	}
	contact, _ := database.GetContactByKey(avatar.Key)
	if contact.Status != "PENDING" {
		_, err := database.CheckContact(avatar.Key, avatar.LocalKey)
		if err != nil {
			fmt.Println(err)
			return c.JSON(http.StatusForbidden, "")
		}
	}
	_, err = database.SaveAvatar(contact, file)
	if err != nil {
		return handleError(c, err)
	}
	return c.JSON(http.StatusCreated, "{}")
}

func newIncomingMessageDeletion(c echo.Context) error {
	ConvertBodyContent(c)
	message := new(data.PeerContactMessage)
	if err := c.Bind(message); err != nil {
		return handleParameterError(c, err)
	}
	if err := c.Validate(message); err != nil {
		return handleParameterError(c, err)
	}
	_, err := database.CheckContact(message.Key, message.LocalKey)
	if err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusForbidden, "")
	}
	localMessage, err := database.GetPost(message.Uuid)
	err = database.DeleteMessage(message.Uuid)
	if err != nil && err.Error() != "not found" {
		return handleError(c, err)
	}
	publishMessageDeletion(c, localMessage)
	return c.JSON(http.StatusNoContent, "")
}
