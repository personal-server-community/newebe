package api

import (
	"errors"
	"fmt"
	"net/http"
	"os"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/personal-server-community/newebe/server/database"
)

const VERSION = "0.1.0"

type (
	Version struct {
		Number   string
		Software string
	}

	Error struct {
		Name string
		Text string
	}

	CustomValidator struct {
		validator *validator.Validate
	}
)

func (cv *CustomValidator) Validate(i interface{}) error {
	return cv.validator.Struct(i)
}

// Manage authentication of every requests. It grabs the given token and
// see if it's stored in the token table. If yes, the request is authorized.
// If not a 403 is returned.
// Token is expected in the Authorization header except for websocket where it
// is send as query param.
func Authenticate(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		token := c.Request().Header.Get("Authorization")
		if token == "" { // Needed for websockets
			token = c.QueryParam("token")
		} else {
			token = token[7:] // Remove "Bearer " prefix
		}
		c.Logger().Info(token)
		_, err := database.GetAuthToken(token)
		if err != nil {
			fmt.Println(err)
			c.Logger().Error(err.Error())
			return c.JSON(http.StatusUnauthorized, "")
		} else {
			return next(c)
		}
	}
}

// Configure JSON object validator of the Echo framework.
// Add a middleware to display logs at the desired format.
func ConfigureMiddlewares(e *echo.Echo) *echo.Echo {
	e.Validator = &CustomValidator{validator: validator.New()}
	// f, err := os.OpenFile("logfile", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	// if err != nil {
	// panic(fmt.Sprintf("error opening file: %v", err))
	// }
	// defer f.Close()
	logger := middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "${time_rfc3339_nano} ${method} ${uri} ${status}\n",
	})
	e.Use(middleware.Recover())
	e.Use(logger)
	return e
}

// Match all routes with Go functions.
func ConfigureRoutes(e *echo.Echo) *echo.Echo {
	jwt_secret, empty := os.LookupEnv("AUTH_JWT_SECRET")
	if !empty {
		panic(errors.New(
			"Auth JWT secret not configured. Please set AUTH_JWT_SECRET environment variable"))
	}
	mediasFolder, empty := os.LookupEnv("MEDIAS_FOLDER")
	if !empty {
		mediasFolder = "medias"
		e.Logger.Info("Folder used for medias: " + mediasFolder)
	}
	clientFolder, empty := os.LookupEnv("CLIENT_FOLDER")
	if !empty {
		clientFolder = "client/newebe/dist"
		e.Logger.Info("Folder used for frontend static filed: " + clientFolder)
	}

	// Audit
	e.GET("/api/version", version)
	e.GET("/api/status", status)

	// Auth
	e.POST("/api/register", register)
	e.POST("/api/login", login)
	e.DELETE("/api/logout", logout)

	// Static files
	e.Static("/", clientFolder)
	e.Static("/api/data/medias", mediasFolder)
	e.GET("/api/tokens", getTokens)

	// Events (webskocket)
	events := e.Group("/api/events")
	events.Use(middleware.JWTWithConfig(middleware.JWTConfig{
		SigningKey:  []byte(jwt_secret),
		TokenLookup: "query:token",
	}))
	events.Use(Authenticate)
	events.GET("", pubsub)

	// Peers
	incoming := e.Group("/api/incoming")
	incoming.POST("/avatar", newIncomingAvatar)
	incoming.POST("/contacts/request", requestContact)
	incoming.POST("/contacts/confirm", confirmedContact)
	incoming.POST("/contacts/revoke", revokedContact)
	incoming.POST("/messages/new", newIncomingMessage)
	incoming.POST("/messages/deletion", newIncomingMessageDeletion)
	incoming.POST("/attachments/new", newIncomingAttachment)

	// Data
	restricted := e.Group("/api/data")
	restricted.Use(middleware.JWT([]byte(jwt_secret)))
	restricted.Use(Authenticate)
	restricted.GET("/authenticated", authenticated)
	restricted.GET("/profile", getUser)
	restricted.PUT("/profile", updateUser)
	restricted.POST("/avatar", setProfilePicture)
	restricted.GET("/contacts", getContacts)
	restricted.POST("/contacts", createContact)
	restricted.POST("/contacts/:id/confirm", confirmContact)
	restricted.DELETE("/contacts/:id", deleteContact)
	restricted.GET("/messages", getMessages)
	restricted.POST("/messages", createMessage)
	restricted.DELETE("/messages/:id", deleteMessage)
	restricted.POST("/messages/with-attachments", createPostWithAttachments)
	restricted.GET("/attachments", getAttachments)

	e.Static("/*", clientFolder)
	return e
}

// Entry point for the server.
func RunServer() {
	// TODO use https://github.com/caarlos0/env
	port, empty := os.LookupEnv("PORT")
	if !empty {
		port = "1323"
	}
	e := echo.New()
	ConfigureMiddlewares(e)
	ConfigureRoutes(e)
	e.Logger.Fatal(e.Start(":" + port))
}
