package main

import (
	"gitlab.com/personal-server-community/newebe/server/api"
)

func main() {
	api.RunServer()
}
